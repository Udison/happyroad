package ru.udison.happyroad;

import java.util.HashMap;
import java.util.Map;

public class Routlist {
    private static Map<Integer, Integer> stopList = new HashMap<>();

    protected static void baseRoute() {
        stopList.put(127, 58);
        stopList.put(124, 55);
        stopList.put(134, 32);
        stopList.put(303, 62);
        stopList.put(142, 36);
        stopList.put(313, 61);
    }

    protected static void setStopList(Integer route, Integer stopCount) {
        stopList.put(route, stopCount);
    }

    public static Map<Integer, Integer> getStopList() {
        return stopList;
    }
}
