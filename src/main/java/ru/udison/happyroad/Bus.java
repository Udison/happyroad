package ru.udison.happyroad;

import java.util.*;

public class Bus {
    private int stops;
    private int route;
    private int ticketPrice;
    private int placeNum;
    public int allMoney;
    private List<Integer> passengerList = new ArrayList<>();

    public Bus(int route, int ticketPrice, int placeNum) {
        this.route = route;
        this.ticketPrice = ticketPrice;
        this.placeNum = placeNum;
    }


    private void setPassengerList(Passenger pas) {
        passengerList.add(pas.stopCount);
    }


    private void setStops(Bus bus) {
        Map<Integer,Integer> check = Routlist.getStopList();
        for(Map.Entry<Integer, Integer> result : check.entrySet()) {
            if (bus.route == result.getKey())
                bus.stops = result.getValue();
        }
    }

    private void addPassenger(Bus bus) {
        Random rand = new Random();
        int freeSeats = bus.placeNum - passengerList.size();
        int enterPass = rand.nextInt(freeSeats);
        for(;enterPass > 0; enterPass--) {
            int stopCount = rand.nextInt(bus.stops);
            bus.setPassengerList(new Passenger(stopCount));
        }
    }

    private void exitPass(Bus bus) {
        Iterator<Integer> iterator = bus.passengerList.iterator();
        while (iterator.hasNext()) {
            if(iterator.next() < 0) {
                iterator.remove();
                bus.allMoney+=ticketPrice;
            }
        }
    }

    private void stopCount (Bus bus) {
            for(int i = 0; i < bus.passengerList.size(); i++) {
                Integer chageNum = (Integer) bus.passengerList.get(i);
                bus.passengerList.set(i, chageNum--);
        }
    }

    public void movement(Bus bus) {
        bus.setStops(bus);
        while (bus.stops > 0) ;
        {
            bus.stops--;
            if (bus.stops > 0) {
                bus.stopCount(bus);
                bus.exitPass(bus);
                bus.addPassenger(bus);
            }
            else return;
        }
    }


}

